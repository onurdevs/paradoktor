
window.onload = function() {
    getCurrency();
    getONS();
}

setInterval(function() {
        getCurrency();
        getONS();
    }, 5000);

function getCurrency() {
    $.ajax({
        url: `https://hasanadiguzel.com.tr/api/kurgetir`,
        type: "GET",
        dataType: "json",
        success: function(data) {
            //console.log(data);
            $(".usdPrice").html(data.TCMB_AnlikKurBilgileri[0].ForexBuying);
            $(".usdChange").html(data.TCMB_AnlikKurBilgileri[0].ForexBuying ?
                `<i class="bi bi-arrow-up text-success"></i>` :
                `<i class="bi bi-arrow-down text-danger"></i>`);

            $(".euroPrice").html(data.TCMB_AnlikKurBilgileri[3].ForexBuying);
            $(".euroChange").html(data.TCMB_AnlikKurBilgileri[3].ForexBuying ?
                `<i class="bi bi-arrow-up text-success"></i>` :
                `<i class="bi bi-arrow-down text-danger"></i>`);

            $(".sterlinPrice").html(data.TCMB_AnlikKurBilgileri[4].ForexBuying);
            $(".sterlinChange").html(data.TCMB_AnlikKurBilgileri[4].ForexBuying ?
                `<i class="bi bi-arrow-up text-success"></i>` :
                `<i class="bi bi-arrow-down text-danger"></i>`);

        },
        error: function(data) {
            console.log(data);
        }
    })
}

function getONS() {
    $.ajax({
        url: "https://hasanadiguzel.com.tr/api/emtiafiyatlari",
        type: "GET",
        dataType: "json",
        success: function(data) {
            //console.log(data);
            $(".onsPrice").html(data.data.kiymetli_metal[0].son);
            $(".onsChange").html(data.data.kiymetli_metal[0].fark > 0 ?
                `<i class="bi bi-arrow-up text-success"></i>` :
                `<i class="bi bi-arrow-down text-danger"></i>`);
            
        },
        error: function(data) {
            console.log(data);
        }
    })
}