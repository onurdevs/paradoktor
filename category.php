<?php require_once("inc/_class/config.php"); ?>
<!DOCTYPE html>
<html lang="tr">

<head>
    <?php 
    include_once 'inc/head.php'; 
    $slug = $_GET['slug'];
    $post = DB::getRow("SELECT * FROM posts WHERE slug = ?", [$slug]);
    ?>
    <title>Para Doktoru - <?=$post->title; ?></title>
</head>

<body data-bs-theme="light">
    <?php include_once 'components/navbar.php'; ?>

    <?php include_once 'components/currencybar.php'; ?>

    <div class="adversting-index-1 text-center">
        <div class="container">
            <div class="row my-2">
                <div class="col-md-12">
                    <div class="py-3 border">
                        advertisement
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="container">
            <div class="row my-3">
                <div class="col-md">
                    <h1 class="text-start text-capitalize">
                        <?=$post->title; ?>
                    </h1>
                    <p class="text-muted">
                        <?=DB::timeTR($post->created_at); ?>
                    </p>

                    <?=$post->content;?>

                </div>
            </div>
        </div>
    </div>
</body>

<script>
$(document).ready(function() {
    $('[data-bs-toggle="tooltip"]').tooltip();

});
</script>
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Article",
    "headline": <?=$post->title;?>,
    "author": "Onur ER",
    "datePublished": <?=date("Y-m-d",strtotime($post->created_at));?>,
    "description": <?=$post->content;?>,
    "image": "https://example.com/image.jpg"
}
</script>


</html>