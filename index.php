<?php require_once("inc/_class/config.php"); ?>
<!DOCTYPE html>
<html lang="tr">

<head>
    <?php include_once 'inc/head.php'; ?>
    <title>Para Doktoru - Anasayfa</title>
</head>

<body data-bs-theme="light">
    <?php include_once 'components/navbar.php'; ?>

    <?php include_once 'components/currencybar.php'; ?>

    <div class="adversting-index-1 text-center">
        <div class="container">
            <div class="row my-2">
                <div class="col-md-12">
                    <div class="py-3 border">
                        advertisement
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div id="carouselExampleCaptions" class="carousel slide">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0"
                                class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                                aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                                aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="https://via.placeholder.com/900x300" class="d-block w-100"
                                    alt="https://via.placeholder.com/900x300">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>First slide label</h5>
                                    <p>Some representative placeholder content for the first slide.</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="https://via.placeholder.com/900x300" class="d-block w-100"
                                    alt="https://via.placeholder.com/900x300">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Second slide label</h5>
                                    <p>Some representative placeholder content for the second slide.</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="https://via.placeholder.com/900x300" class="d-block w-100"
                                    alt="https://via.placeholder.com/900x300">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Third slide label</h5>
                                    <p>Some representative placeholder content for the third slide.</p>
                                </div>
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>

                <div class="col-md">
                    <div class="advertisement-index-2">
                        <div class="py-3 border">
                            advertisement
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="blog-index">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-9">
                    <?php  
                        $posts = DB::get("SELECT * FROM posts ORDER BY id DESC LIMIT 3");
                        foreach($posts as $post) :
                    ?>
                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title fw-bold text-capitalize"><?=$post->title;?></h5>
                            <p class="card-text">
                                <?=strlen($post->content) > 200 ? substr($post->content, 0, 200) . "..." : $post->content;?>
                            </p>
                            <a href="detailsPost.php?slug=<?=$post->slug;?>" class="btn btn-primary btn-sm">
                                <i class="bi bi-arrow-right"></i> Devamını Oku
                            </a>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="col-md">
                    <div class="list-group">
                        <!-- title -->
                        <a href="#" class="list-group-item list-group-item-action disabled fw-bold" aria-current="true">
                            Kategorilerss
                        </a>
                        <?php 
                            $categories = DB::get("SELECT title, count(*) as total FROM categories ORDER BY id DESC");
                            foreach($categories as $category) :
                        ?>
                        <a href="detailsCategory.php?slug=<?=$category->slug;?>"
                            class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                            <?=$category->title;?>
                            <span class="badge bg-primary rounded-pill"><?=$category->total;?></span>
                        </a>
                        <?php endforeach; ?>


                    </div>

                </div>
            </div>
        </div>
    </div>
</body>

<script>
$(document).ready(function() {
    $('[data-bs-toggle="tooltip"]').tooltip();

});
</script>


</html>