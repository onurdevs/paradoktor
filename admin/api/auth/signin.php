<?php require_once("../../../inc/_class/config.php");

if(!DB::csrfControl($_POST["request_id"])) {
    die("CSRF Hatası!");
}

if(!isset($_POST["username"]) || !isset($_POST["password"])) {
    die("Lütfen kullanıcı adı ve şifre giriniz.");
}

$username = $_POST["username"];
$password = $_POST["password"];

$userControl = DB::getRow("SELECT username, password FROM users WHERE username = ?", [$username]);

if(!$userControl) {
    die("<div class='alert alert-danger'>Kullanıcı adı veya şifre hatalı.</div>");
}

if(!password_verify($password, $userControl->password)) {
    die("<div class='alert alert-danger'>Kullanıcı adı veya şifre hatalı.</div>");
}

//insert cookie key database

$cookieKey = md5(uniqid(rand(), true));
$updateCookie = DB::exec("UPDATE users SET cookie_key = ? WHERE username = ?", [$cookieKey, $username]);

if(!$updateCookie) {
    die("<div class='alert alert-danger'>Çerez anahtarı oluşturulamadı.</div>");
}

setcookie("cookie_key", $cookieKey, time() + (86400 * 30), "/");

if(isset($_POST["remember"])) {
    setcookie("remember", "1", time() + (86400 * 30), "/");
}

echo "<div class='alert alert-success'>Giriş başarılı. Yönlendiriliyorsunuz...</div>";
echo '<script>window.location.href = "dashboard.php";</script>';

?>


