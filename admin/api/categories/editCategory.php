<?php require_once("../../../inc/_class/config.php");

if(!DB::csrfControl($_POST["request_id"])) {
    die("CSRF Hatası!");
}

if(!isset($_POST['title']) || !isset($_POST['id'])){
    die("Lütfen tüm alanları doldurunuz.");
}

$id = $_POST['id'];
$title = $_POST['title'];
$slug = DB::seo($title);

$updateCategory = DB::exec("UPDATE categories SET title = ?, slug = ? WHERE id = ?", [$title, $slug, $id]);

if(!$updateCategory) {
    die("<div class='alert alert-danger'>Kategori düzenlenemedi.</div>");
}

echo "<div class='alert alert-success'>Kategori başarıyla düzenlendi. Yönlendiriliyorsunuz...</div>";

echo '<script>window.location.href = "listCategories.php";</script>';

?>