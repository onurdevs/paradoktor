<?php require_once("../../../inc/_class/config.php");

if(!DB::csrfControl($_POST["request_id"])) {
    die("CSRF Hatası!");
}

if(!isset($_POST['title'])){
    die("Lütfen tüm alanları doldurunuz.");
}

$title = $_POST['title'];
$slug = DB::seo($title);

$insertCategory = DB::exec("INSERT INTO categories SET title = ?, slug = ?", [$title, $slug]);

if(!$insertCategory) {
    die("<div class='alert alert-danger'>Kategori oluşturulamadı.</div>");
}

echo "<div class='alert alert-success'>Kategori başarıyla oluşturuldu. Yönlendiriliyorsunuz...</div>";

echo '<script>window.location.href = "listCategories.php";</script>';