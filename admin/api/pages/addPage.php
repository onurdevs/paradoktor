<?php require_once("../../../inc/_class/config.php");

if(!DB::csrfControl($_POST["request_id"])) {
    die("CSRF Hatası!");
}

if(!isset($_POST["title"]) || !isset($_POST["content"])) {
    die("Lütfen tüm alanları doldurunuz.");
}

$title = $_POST["title"];
$content = $_POST["content"];
$slug = DB::seo($title);

$insertPost = DB::exec("INSERT INTO pages SET title = ?, content = ?, slug= ?", [$title, $content, $slug]);

if(!$insertPost) {
    die("<div class='alert alert-danger'>Sayfa oluşturulamadı.</div>");
}

echo "<div class='alert alert-success'>Sayfa başarıyla oluşturuldu. Yönlendiriliyorsunuz...</div>";
echo '<script>window.location.href = "listPages.php";</script>';