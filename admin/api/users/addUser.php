<?php require_once("../../../inc/_class/config.php");

if(!DB::csrfControl($_POST["request_id"])) {
    die("CSRF Hatası!");
}

if(!isset($_POST['name']) || !isset($_POST['username']) || !isset($_POST['password']) || !isset($_POST['roles'])){
    die("Lütfen tüm alanları doldurunuz.");
}

$name = $_POST['name'];
$username = $_POST['username'];
$password = DB::hashMake($_POST['password']);
$roles = $_POST['roles'];

$insertUser = DB::exec("INSERT INTO users SET name = ?, username = ?, password = ?, roles = ?", [$name, $username, $password, $roles]);

if(!$insertUser) {
    die("<div class='alert alert-danger'>Kullanıcı oluşturulamadı.</div>");
}

echo "<div class='alert alert-success'>Kullanıcı başarıyla oluşturuldu. Yönlendiriliyorsunuz...</div>";

echo '<script>window.location.href = "listUsers.php";</script>';