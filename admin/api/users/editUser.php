<?php require_once("../../../inc/_class/config.php");

if(!DB::csrfControl($_POST["request_id"])) {
    die("CSRF Hatası!");
}


if(!isset($_POST['name']) || !isset($_POST['username']) || !isset($_POST['roles'])){
    die("Lütfen tüm alanları doldurunuz.");
}

$name = $_POST['name'];
$username = $_POST['username'];
$roles = $_POST['roles'];

if(isset($_POST['password']) && !empty($_POST['password'])) {
    $password = DB::hashMake($_POST['password']);
    $updateUser = DB::exec("UPDATE users SET name = ?, username = ?, password = ?, roles = ? WHERE username = ?", [$name, $username, $password, $roles, $_POST["username"]]);
} else {
    $updateUser = DB::exec("UPDATE users SET name = ?, username = ?, roles = ? WHERE username = ?", [$name, $username, $roles, $_POST["username"]]);
}

if(!$updateUser) {
    die("<div class='alert alert-danger'>Kullanıcı düzenlenemedi.</div>");
}

echo "<div class='alert alert-success'>Kullanıcı başarıyla düzenlendi. Yönlendiriliyorsunuz...</div>";

echo '<script>window.location.href = "listUsers.php";</script>';