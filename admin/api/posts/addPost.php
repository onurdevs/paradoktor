<?php require_once("../../../inc/_class/config.php");

if(!DB::csrfControl($_POST["request_id"])) {
    die("CSRF Hatası!");
}

if(!isset($_POST["title"]) || !isset($_POST["content"]) || !isset($_POST["category_id"])) {
    die("Lütfen tüm alanları doldurunuz.");
}

$title = $_POST["title"];
$content = $_POST["content"];
$category_id = $_POST["category_id"];
$slug = DB::seo($title);

$insertPost = DB::exec("INSERT INTO posts SET title = ?, content = ?, category = ?, slug = ?", [$title, $content, $category_id, $slug]);

if(!$insertPost) {
    die("<div class='alert alert-danger'>Gönderi oluşturulamadı.</div>");
}

echo "<div class='alert alert-success'>Gönderi başarıyla oluşturuldu. Yönlendiriliyorsunuz...</div>";
echo '<script>window.location.href = "listPosts.php";</script>';

?>