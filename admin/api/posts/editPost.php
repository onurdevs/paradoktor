<?php require_once("../../../inc/_class/config.php");

if(!DB::csrfControl($_POST["request_id"])) {
    die("CSRF Hatası!");
}

if(!isset($_POST["title"]) || !isset($_POST["content"]) || !isset($_POST["category_id"])) {
    die("Lütfen tüm alanları doldurunuz.");
}

$title = $_POST["title"];
$content = $_POST["content"];
$category_id = $_POST["category_id"];
$slug = DB::seo($title);

$updatePost = DB::exec("UPDATE posts SET title = ?, content = ?, slug= ?, category = ? WHERE slug = ?", [$title, $content, $slug, $category_id, $_POST["slug"]]);

if(!$updatePost) {
    die("<div class='alert alert-danger'>Gönderi düzenlenemedi.</div>");
}

echo "<div class='alert alert-success'>Gönderi başarıyla düzenlendi. Yönlendiriliyorsunuz...</div>";
echo '<script>window.location.href = "listPosts.php";</script>';



