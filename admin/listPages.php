<?php require_once("../inc/_class/config.php"); require_once "authControl.php"; ?>
<!doctype html>
<html lang="tr">

<head>
    <?php require_once("inc/head.php"); ?>
    <title>Document</title>
</head>

<body>
    <?php 
    
    include_once("page/navbar.php"); 
    $pages = DB::get("SELECT * FROM pages");

    ?>

    <div class="container">
        <div class="row mt-3">
            <div class="col-md">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <a href="addPage.php"
                                        class="btn btn-sm btn-success d-block d-md-inline mb-2 mb-md-0">Yeni Sayfa
                                        Oluştur</a>
                                </th>
                            </tr>
                            <tr>
                                <th>Başlık</th>
                                <th>Oluşturulma Tarihi</th>
                                <th>İşlem</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($pages as $page): ?>
                            <tr>
                                <td class="align-middle"><?=$page->title;?></td>
                                <td class="align-middle">
                                    <?=date("d.m.Y H:i", strtotime($page->created_at))?>
                                </td>
                                <td style="width:20px" class="align-middle">
                                    <div class="btn-group btn-group-sm">
                                        <a href="editPage.php?slug=<?=$page->slug;?>" class="btn btn-primary d-block d-md-inline mb-2 mb-md-0">Düzenle</a>
                                        <a href="#" class="btn btn-danger d-block d-md-inline ">Sil</a>
                                    </div>
                                </td>
                            </tr>
                            <?php   endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php require_once("inc/script.php"); ?>
</body>

</html>