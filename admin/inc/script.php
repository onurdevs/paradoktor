<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.form.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>

<script src="https://cdn.ckeditor.com/ckeditor5/39.0.0/classic/ckeditor.js"></script>

<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );

</script>
<script src="assets/js/master.js"></script>