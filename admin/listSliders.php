<?php require_once("../inc/_class/config.php"); require_once "authControl.php"; ?>
<!doctype html>
<html lang="tr">

<head>
    <?php require_once("inc/head.php"); ?>
    <title>Document</title>
</head>

<body>
    <?php 
    
    include_once("page/navbar.php"); 
    $sliders = DB::get("SELECT * FROM sliders");

    ?>

    <div class="container">
        <div class="row mt-3">
            <div class="col-md">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <button type="button" data-bs-toggle="modal" data-bs-target="#exampleModal"
                                        class="btn btn-sm btn-success d-block d-md-inline mb-2 mb-md-0">Yeni Slider
                                        Oluştur</button>
                                </th>
                            </tr>
                            <tr>
                                <th>Başlık</th>
                                <th>Oluşturulma Tarihi</th>
                                <th>İşlem</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($sliders as $slider): ?>
                            <tr>
                                <td class="align-middle"><?=$slider->title;?></td>
                                <td class="align-middle">
                                    <?=date("d.m.Y H:i", strtotime($slider->created_at))?>
                                </td>
                                <td style="width:20px" class="align-middle">
                                    <div class="btn-group btn-group-sm">
                                        <a href="editslider.php?slug=<?=$slider->slug;?>" class="btn btn-primary d-block d-md-inline mb-2 mb-md-0">Düzenle</a>
                                        <a href="#" class="btn btn-danger d-block d-md-inline ">Sil</a>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>
            <div class="mb-3">
                <label class="form-label">Başlık</label>
                <input type="text" class="form-control">
            </div>
            <div class="mb-3">
                <label class="form-label">Açıklama</label>
                <input type="text" class="form-control">
            </div>
            <div class="mb-3">
                <label class="form-label">Slider Resmi</label>
                <input type="file" class="form-control">
            </div>
            <div class="mb-3">
                <label class="form-label">Slider Linki</label>
                <input type="text" value="#" class="form-control">
            </div>
            <div class="">
                <button type="submit" class="btn btn-primary">Kaydet</button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Kapat</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

    <?php require_once("inc/script.php"); ?>
</body>

</html>