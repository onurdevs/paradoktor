<?php require_once("../inc/_class/config.php"); require_once "authControl.php"; ?>
<!doctype html>
<html lang="tr">

<head>
    <?php require_once("inc/head.php"); ?>
    <title>Document</title>
</head>

<body>
    <?php include_once("page/navbar.php"); ?>

    <div class="container">
        <div class="row mt-3">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Kategori Oluştur</h5>
                        <form id="frmAddCategory" action="api/categories/addCategory.php" method="post">
                            <div class="mb-3">
                                <label for="title" class="form-label">Başlık</label>
                                <input type="text" class="form-control" id="title" name="title">
                            </div>
                            <input type="hidden" name="request_id" value="<?=DB::csrf();?>">
                            <button type="submit" class="btn btn-primary">Kaydet</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require_once("inc/script.php"); ?>
    <script type="text/javascript">
    $("#frmAddCategory").ajaxForm({
        target: "#frmAddCategory"
    })
    </script>

</body>

</html>