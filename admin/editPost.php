<?php require_once("../inc/_class/config.php"); require_once "authControl.php"; ?>
<!doctype html>
<html lang="tr">

<head>
    <?php require_once("inc/head.php"); ?>
    <title>Document</title>
</head>

<body>
    <?php include_once("page/navbar.php"); 
    $categories = DB::get("SELECT * FROM categories");
    $slug = $_GET["slug"];
    $post = DB::getRow("SELECT * FROM posts WHERE slug = ?", [$slug]);

    ?>

    <div class="container">
        <div class="row mt-3">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Gönderi Düzenle</h5>
                        <form id="frmAddPost" action="api/posts/editPost.php" method="post">
                            <div class="mb-3">
                                <label class="form-label">Kategori</label>
                                <select class="form-select" name="category_id">
                                    <?php foreach($categories as $category): ?>
                                    <option <?=$post->category === $category->id ? "selected" : null; ?> value="<?=$category->id;?>"><?=$category->title;?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="title" class="form-label">Başlık</label>
                                <input type="text" class="form-control" value="<?=$post->title;?>" id="title" name="title">
                            </div>
                            <div class="mb-3">
                                <label for="content" class="form-label">İçerik</label>
                                <textarea class="form-control" id="editor" name="content">
                                    <?=$post->content;?>
                                </textarea>
                            </div>
                            <input type="hidden" name="slug" value="<?=$post->slug;?>">
                            <input type="hidden" name="request_id" value="<?=DB::csrf();?>" />
                            <button type="submit" class="btn btn-primary">Kaydet</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require_once("inc/script.php"); ?>
    <script type="text/javascript">
    $("#frmAddPost").ajaxForm({
        target: "#frmAddPost"
    })
    </script>
</body>

</html>