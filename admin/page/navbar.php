<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container">
        <a class="navbar-brand" href="#"><span class="fw-bold">w</span>panel</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Başlangıç</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Sayfa Yönetimi
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="listPages.php">Sayfalar</a></li>
                        <li><a class="dropdown-item" href="addPage.php">Sayfa Oluştur</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Blog Yönetimi
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="listCategories.php">Kategoriler</a></li>
                        <li><a class="dropdown-item" href="listPosts.php">Gönderiler</a></li>
                        <li><a class="dropdown-item" href="addPost.php">Gönderi Oluştur</a></li>
                    </ul>
                </li>
                <?php 
                
                $roleControl = DB::getRow("SELECT * FROM users WHERE cookie_key = ?", [$_COOKIE["cookie_key"]]);
                if($roleControl->roles == "admin"){
                    
                ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Kullanıcı Yönetimi
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="listUsers.php">Kullanıcı Listesi</a></li>
                        <li><a class="dropdown-item" href="addUser.php">Kullanıcı Oluştur</a></li>
                    </ul>
                </li>
                <?php } ?>
                <li class="nav-item">
                    <a class="nav-link" href="listSliders.php">Slider Yönetimi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="logout.php">Çıkış Yap</a>
                </li>
            </ul>
        </div>
    </div>
</nav>