<?php require_once "../inc/_class/config.php";

if(!isset($_COOKIE["cookie_key"])) {
    header("Location: authControl.php");
    exit;
}

$cookieKey = $_COOKIE["cookie_key"];

$userControl = DB::getRow("SELECT username, cookie_key FROM users WHERE cookie_key = ?", [$cookieKey]);

if(!$userControl) {
    header("Location: authControl.php");
    exit;
}

$updateControl = DB::exec("UPDATE users SET cookie_key = ? WHERE username = ?", [null, $userControl->username]);

if(!$updateControl) {
    die("Çerez anahtarı silinemedi.");
}

setcookie("cookie_key", null, time() - (86400 * 30), "/");
setcookie("remember", null, time() - (86400 * 30), "/");
header("Location: authControl.php");
exit;

?>