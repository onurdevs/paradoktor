<?php require_once("../inc/_class/config.php"); ?>
<!doctype html>
<html lang="tr">
<head>
    <?php require_once("inc/head.php"); ?>
    <link rel="stylesheet" href="assets/css/signin.css">
    <title>Document</title>
</head>
<body class="vh-100 d-flex flex-column justify-content-center align-items-center bg-light">

    <div class="form-signin shadow-sm">
        <form class="form-signin" id="frmLogin" action="api/auth/signin.php" method="post">
            <div class="mb-3">
                <img src="assets/images/logo.png" alt="Logo" class="img-fluid">
            </div>
            <div class="mb-3">
                <input type="text" class="form-control" name="username" placeholder="Kullanıcı Adı">
            </div>
            <div class="mb-3">
                <input type="password" class="form-control" name="password" placeholder="Şifre">
            </div>
            <div class="mb-3">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="remember" id="remember">
                    <label for="remember" class="form-check-label">Beni Hatırla</label>
                </div>
            </div>
            <div class="mb-3">
                <input type="hidden" name="request_id" value="<?=DB::csrf();?>" />
                <button type="submit" class="btn btn-primary btn-block">Giriş Yap</button>
            </div>
        </form>
    </div>

    <?php require_once("inc/script.php"); ?>
    <script>
        $(".form-signin").ajaxForm(
            {
                target: "#frmLogin"
            }
        )
    </script>
</body>
</html>