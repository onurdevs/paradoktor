<?php require_once("../inc/_class/config.php"); require_once "authControl.php"; ?>
<!doctype html>
<html lang="tr">

<head>
    <?php require_once("inc/head.php"); ?>
    <title>Document</title>
</head>

<body>
    <?php 
    
    include_once("page/navbar.php"); 
    $categories = DB::get("SELECT * FROM categories");

    ?>

    <div class="container">
        <div class="row mt-3">
            <div class="col-md">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <a href="addCategory.php"
                                        class="btn btn-sm btn-success d-block d-md-inline mb-2 mb-md-0">Yeni Kategori
                                        Oluştur</a>
                                </th>
                            </tr>
                            <tr>
                                <th>Başlık</th>
                                <th>Oluşturulma Tarihi</th>
                                <th>İşlem</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($categories as $category): ?>
                            <tr>
                                <td class="align-middle"><?=$category->title;?></td>
                                <td class="align-middle">
                                    <?=date("d.m.Y H:i", strtotime($category->created_at))?>
                                </td>
                                <td style="width:20px" class="align-middle">
                                    <div class="btn-group btn-group-sm">
                                        <a href="editCategory.php?slug=<?=$category->slug;?>" class="btn btn-primary d-block d-md-inline mb-2 mb-md-0">Düzenle</a>
                                        <a href="#" class="btn btn-danger d-block d-md-inline ">Sil</a>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php require_once("inc/script.php"); ?>
</body>

</html>