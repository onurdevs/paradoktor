<?php require_once("../inc/_class/config.php"); require_once "authControl.php"; ?>
<!doctype html>
<html lang="tr">

<head>
    <?php require_once("inc/head.php"); ?>
    <title>Document</title>
</head>

<body>
    <?php 
        include_once("page/navbar.php"); 
        $slug = $_GET["slug"];
        $category = DB::getRow("SELECT * FROM categories WHERE slug = ?", [$slug]);
    ?>

    <div class="container">
        <div class="row mt-3">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Kategori Düzenle</h5>
                        <form id="frmEditCategory" action="api/categories/editCategory.php" method="post">
                            <div class="mb-3">
                                <label for="title" class="form-label">Başlık</label>
                                <input type="text" class="form-control" value="<?=$category->title;?>" id="title" name="title">
                            </div>
                            <input type="hidden" name="id" value="<?=$category->id;?>">
                            <input type="hidden" name="request_id" value="<?=DB::csrf();?>">
                            <button type="submit" class="btn btn-primary">Kaydet</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require_once("inc/script.php"); ?>
    <script type="text/javascript">
    $("#frmEditCategory").ajaxForm({
        target: "#frmEditCategory"
    })
    </script>

</body>

</html>