<?php require_once "../inc/_class/config.php";

if(!isset($_COOKIE["cookie_key"])) {
    header("Location: /admin");
    exit;
}

$cookieKey = $_COOKIE["cookie_key"];

$userControl = DB::getRow("SELECT username, cookie_key FROM users WHERE cookie_key = ?", [$cookieKey]);

if(!$userControl) {
    header("Location: authControl.php");
    exit;
}

?>