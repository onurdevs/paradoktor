<?php require_once("../inc/_class/config.php"); require_once "authControl.php"; ?>
<!doctype html>
<html lang="tr">

<head>
    <?php require_once("inc/head.php"); ?>
    <title>Document</title>
</head>

<body>
    <?php include_once("page/navbar.php"); ?>

    <div class="container">
        <div class="row mt-3">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Kullanıcı Oluştur</h5>
                        <form id="frmAddUser" action="api/users/addUser.php" method="post">
                            <div class="mb-3">
                                <label for="title" class="form-label">Başlık</label>
                                <select class="form-select" name="roles">
                                    <option value="admin">Yönetici</option>
                                    <option value="editor">Editör</option>
                                    <option value="user">Kullanıcı</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="title" class="form-label">Adı</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="mb-3">
                                <label for="title" class="form-label">Kullanıcı Adı</label>
                                <input type="text" class="form-control" id="username" name="username">
                            </div>
                            <div class="mb-3">
                                <label for="title" class="form-label">Şifre</label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                            <input type="hidden" name="request_id" value="<?=DB::csrf();?>">
                            <button type="submit" class="btn btn-primary">Kaydet</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require_once("inc/script.php"); ?>
    <script type="text/javascript">
    $("#frmAddUser").ajaxForm({
        target: "#frmAddUser"
    })
    </script>

</body>

</html>