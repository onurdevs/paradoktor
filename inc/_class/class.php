<?php
class DB {
	static $pdo = null;
	static $charset = 'UTF8';
	static $last_stmt = null;
	public static function instance(){
		return 
		self::$pdo == null ?
		self::init() :
		self::$pdo;
	}
	public static function init(){
		self::$pdo = new PDO('mysql:host=' . MYSQL_HOST .';dbname=' . MYSQL_DB,	MYSQL_USER,	MYSQL_PASS	);
		self::$pdo->exec('SET NAMES `' . self::$charset . '`');
		self::$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
		return self::$pdo;
	}
	public static function query($query, $bindings = null){
		if(is_null($bindings)){
			if(!self::$last_stmt = self::instance()->query($query))
			return false;
		}else{
			self::$last_stmt = self::prepare($query);
			if(!self::$last_stmt->execute($bindings))
			return false;
		}
		return self::$last_stmt;
	}
	public static function getVar($query, $bindings = null){
		if(!$stmt = self::query($query, $bindings))
		return false;
		
		return $stmt->fetchColumn();
	}
	public static function getRow($query, $bindings = null){
		if(!$stmt = self::query($query, $bindings))
		return false;
		return $stmt->fetch();
	}
	public static function get($query, $bindings = null){
		if(!$stmt = self::query($query, $bindings))
			return false;

		$result = array();
		foreach($stmt as $row)
			$result[] = $row;

		return $result;
	}

	public static function exec($query, $bindings = null){
		if(!$stmt = self::query($query, $bindings))
		return false;
		return $stmt->rowCount();
	}

	public static function insert($query, $bindings = null){
		if(!$stmt = self::query($query, $bindings))
		return false;
		
		return self::$pdo->lastInsertId();
	}

	public static function getLastError(){
		$error_info = self::$last_stmt->errorInfo();

		if($error_info[0] == 00000)
			return false;

		return $error_info;
	}

	public static function __callStatic($name, $arguments){
		return call_user_func_array(
			array(self::instance(), $name),
			$arguments
		);
	}
	public static function timeTR($par){
		$explode = explode(" ", $par);
		$explode2 = explode("-", $explode[0]);
		$zaman = substr($explode[1], 0, 5);
		if ($explode2[1] == "1") $ay = "Ocak";
		elseif ($explode2[1] == "2") $ay = "Şubat";
		elseif ($explode2[1] == "3") $ay = "Mart";
		elseif ($explode2[1] == "4") $ay = "Nisan";
		elseif ($explode2[1] == "5") $ay = "Mayıs";
		elseif ($explode2[1] == "6") $ay = "Haziran";
		elseif ($explode2[1] == "7") $ay = "Temmuz";
		elseif ($explode2[1] == "8") $ay = "Ağustos";
		elseif ($explode2[1] == "9") $ay = "Eylül";
		elseif ($explode2[1] == "10") $ay = "Ekim";
		elseif ($explode2[1] == "11") $ay = "Kasım";
		elseif ($explode2[1] == "12") $ay = "Aralık";
		return $zaman.", ".$explode2[2]." ".$ay." ".$explode2[0];
	}
	public static function hashMake($str) {
		$hashedPassword = password_hash(trim($str), PASSWORD_DEFAULT);
		return $hashedPassword;
	}
	public static function sanitizeInput($value) {
		if (is_array($value)) {
			return array_map(array($this, 'sanitizeInput'), $value);
		}

		$value = trim($value);
		$value = stripslashes($value);		
		$value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
		return $value;
	}

	public static function control($theValue, $theType, $theDefinedValue = null, $theNotDefinedValue = null) {
		$theValue = DB::sanitizeInput($theValue);

		switch ($theType) {
			case "date":
			case "text":
				$theValue = ($theValue !== "") ? "'" . $theValue . "'" : "NULL";
				break;
			case "long":
			case "int":
				$theValue = ($theValue !== "") ? intval($theValue) : "NULL";
				break;
			case "double":
				$theValue = ($theValue !== "") ? "'" . doubleval($theValue) . "'" : "NULL";
				break;
			case "defined":
				$theValue = ($theValue !== "") ? $theDefinedValue : $theNotDefinedValue;
				break;
		}
		return $theValue;
	}
	public static function seo($title){
		$TR=array('ç','Ç','ı','İ','ş','Ş','ğ','Ğ','ö','Ö','ü','Ü');
		$EN=array('c','c','i','i','s','s','g','g','o','o','u','u');
		$title= str_replace($TR,$EN,$title);
		$title=mb_strtolower($title,'UTF-8');
		$title=preg_replace('#[^-a-zA-Z0-9_, ]#','',$title);
		$title=trim($title);
		$title= preg_replace('#[-_ ]+#','-',$title);
		return $title;
	}
	
	public static function getIP(){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])){ $ip = $_SERVER['HTTP_CLIENT_IP']; }
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){ $ip = $_SERVER['HTTP_X_FORWARDED_FOR']; }
		else{ $ip = $_SERVER['REMOTE_ADDR']; }
		return $ip;
	}
	
	
	public static function fileUpload($location,$ext,$name,$target){
		$return = null;
		$avaible = null;
		
		$exists = strtolower(substr($ext, strrpos($ext, ".")));
		$avaible = array(".jpg", ".jpeg", ".png", ".bmp", ".gif");
		if(in_array($exists, $avaible)){ 
			$new_name = $name.$exists;	
			$to_location = $target.'/'.$new_name;
			if (move_uploaded_file($location, $to_location)){ $return = $new_name; }
		}else{ $return = null; }
		return DB::control($return,'text');
	}

	public static function timeago($date){
		if(empty($date)) { return "zamansız bir durum"; }	
		$periods = array("saniye", "dakika", "saat", "gün", "hafta", "ay", "yıl", "onyıl");
		$lengths = array("60","60","24","7","4.35","12","10");
		$now = time();
		$unix_date = strtotime($date);
		if(empty($unix_date)) { return "zaman kötü"; }
		if($now > $unix_date) {
			$difference = $now - $unix_date;
			$tense = "önce";
		} else {
			$difference = $unix_date - $now;
			$tense = "şu andan itibaren";
		}
		for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			$difference /= $lengths[$j];
		}
		$difference = round($difference);
		
		return "$difference $periods[$j] {$tense}";
	}
	public static function sendMail ($sender, $sender_name, $receiver, $subject, $html){
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->Host = SMTP_HOST;
		$mail->SMTPSecure = SMTP_HOST;
		$mail->Port = 587;
		$mail->Username = SMTP_EMAIL;
		$mail->Password = SMTP_PASSWORD;
		$mail->SetFrom($sender, $sender_name);
		$mail->AddAddress($receiver);
		$mail->CharSet = 'UTF-8';
		$mail->Subject = $subject;
		$mail->MsgHTML($html);
		return $mail->Send();
	}
	public static function reArrayFiles($file_post) {

	    $file_ary = array();
	    $file_count = count($file_post['name']);
	    $file_keys = array_keys($file_post);

	    for ($i=0; $i<$file_count; $i++) {
	        foreach ($file_keys as $key) {
	            $file_ary[$i][$key] = $file_post[$key][$i];
	        }
	    }

	    return $file_ary;
	}	
	
	public static function getCurrency($type){
		$connect_web = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');
		$usd_selling = $connect_web->Currency[0]->BanknoteSelling;
		$euro_selling = $connect_web->Currency[3]->BanknoteSelling;
 		
 		$return = ($type=='eur') ? $euro_selling : $usd_selling;
 		return $return;
	}

	public static function key($length = 10){

	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;

	}

	public static function csrf(){
		$csrf = md5(uniqid(rand(), true));
		$_SESSION['csrf'] = $csrf;
		return $csrf;
	}

	public static function csrfControl($csrf){
		if(isset($_SESSION['csrf']) && $csrf == $_SESSION['csrf']){
			unset($_SESSION['csrf']);
			return true;
		}else{
			return false;
		}
	}

}


?>
