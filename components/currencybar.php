<div class="bg-body-secondary">
        <div class="container">
            <div class="row py-2">
                <div class="col-md-2">
                    <span class="small text-muted">Ons Altın</span>
                    <h6 class="m-0">
                        <span class="onsPrice"></span>
                        <span class="onsChange"></span>
                    </h6>
                </div>
                <div class="col-md-2">
                    <span class="small text-muted">Dolar</span>
                    <h6 class="m-0">
                        <span class="usdPrice"></span>
                        <span class="usdChange"></span>
                    </h6>
                </div>
                <div class="col-md-2">
                    <span class="small text-muted">Euro</span>
                    <h6 class="m-0">
                        <span class="euroPrice"></span>
                        <span class="euroChange"></span>
                    </h6>
                </div>
                <div class="col-md-2">
                    <span class="small text-muted">Sterlin</span>
                    <h6 class="m-0">
                        <span class="sterlinPrice"></span>
                        <span class="sterlinChange"></span>
                    </h6>
                </div>
                <div class="col-md-2">
                    <span class="small text-muted">BIST100</span>
                    <h6 class="m-0">1.800$</h6>
                </div>
            </div>
        </div>
    </div>